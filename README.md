<a href="https://flathub.org/apps/details/org.gnome.design.SymbolicPreview">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Symbolic Preview

<img src="https://gitlab.gnome.org/World/design/symbolic-preview/raw/master/data/icons/org.gnome.design.SymbolicPreview.svg" width="128" height="128" />
<p>Symbolics made easy.</p>

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Symbolic Preview
To build the development version of Symbolic Preview and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow the GNOME Code of Conduct when participating in project
spaces.


mod category;
mod icon;
mod utils;

pub use category::Category;
pub use icon::Icon;

pub mod icons_set;

pub static SYMBOLIC_CSS: &str = r#"
.error{fill:#e01b24 !important;}
.success{fill:#33d17a !important;}
.warning{fill:#ff7800 !important;}
"#;

pub use utils::{
    clean_svg, random_symbolics, replace_classes_with_fill, replace_fill_with_classes,
    validate_project_name,
};

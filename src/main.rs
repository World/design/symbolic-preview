mod icons;
mod widgets;

mod application;
mod config;
mod project;
mod recents;
mod utils;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};
use gettextrs::*;
use gtk::{gio, glib};

fn main() -> glib::ExitCode {
    tracing_subscriber::fmt::init();

    glib::set_application_name(&format!("Symbolic Preview{}", config::NAME_SUFFIX));

    gtk::init().expect("Unable to start GTK");
    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).unwrap();
    textdomain(GETTEXT_PACKAGE).unwrap();

    let res = gio::Resource::load(config::RESOURCES_FILE).expect("Could not load resources");
    gio::resources_register(&res);

    Application::run()
}

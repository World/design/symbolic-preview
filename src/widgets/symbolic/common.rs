use std::path::Path;

pub fn icon_name(icon_path: &Path) -> String {
    let icon_name = icon_path.file_name();
    let icon_name = icon_name
        .unwrap_or_else(|| std::ffi::OsStr::new(""))
        .to_str()
        .unwrap();
    icon_name.trim_end_matches(".svg").to_string()
}

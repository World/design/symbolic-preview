// Create a new Icon Set Dialog
use std::path::PathBuf;

use adw::{prelude::*, subclass::prelude::*};
use gettextrs::gettext;
use gtk::glib;

use crate::{
    icons::validate_project_name,
    project::{Project, ProjectType},
    widgets::{FileButton, Window},
};

mod imp {
    use std::cell::Cell;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::NewProjectWindow)]
    #[template(resource = "/org/gnome/design/SymbolicPreview/new_project.ui")]
    pub struct NewProjectWindow {
        #[template_child]
        pub(super) name_desc_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub name_row: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub source_row: TemplateChild<adw::ActionRow>,
        #[template_child]
        pub(super) create_btn: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) file_button: TemplateChild<FileButton>,
        #[property(get, set, construct_only, builder(ProjectType::default()))]
        pub(super) project_type: Cell<ProjectType>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for NewProjectWindow {
        const NAME: &'static str = "NewProjectWindow";
        type Type = super::NewProjectWindow;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_instance_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for NewProjectWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            let project_type = self.obj().project_type();
            match project_type {
                ProjectType::Symbolic => {
                    obj.set_title(Some(&gettext("New Symbolic Icon")));
                    self.name_row.set_title(&gettext("Icon Name"));
                    self.source_row.set_title(&gettext("Icon Source Location"));
                    self.name_desc_label.set_visible(true);
                }
                ProjectType::IconsSet => {
                    obj.set_title(Some(&gettext("New Icons Set")));
                    self.name_row.set_title(&gettext("Icons Set Name"));
                    self.source_row
                        .set_title(&gettext("Icons Set Source Location"));
                    self.name_desc_label.set_visible(false);
                    self.name_row.set_text("icons-set");
                }
            };
        }
    }
    impl WidgetImpl for NewProjectWindow {}
    impl WindowImpl for NewProjectWindow {}
    impl AdwWindowImpl for NewProjectWindow {}
}

glib::wrapper! {
    pub struct NewProjectWindow(ObjectSubclass<imp::NewProjectWindow>)
        @extends gtk::Widget, gtk::Window, adw::Window,
        @implements gtk::Root, gtk::Native;
}

#[gtk::template_callbacks]
impl NewProjectWindow {
    pub fn new(project_type: ProjectType) -> Self {
        glib::Object::builder()
            .property("project-type", project_type)
            .build()
    }

    #[template_callback]
    fn on_selected_file_validate(
        _file_button: glib::Object,
        _pspec: glib::ParamSpec,
        _self: &NewProjectWindow,
    ) {
        _self.validate_contents();
    }

    #[template_callback]
    fn on_name_changed(&self, _entry: adw::EntryRow) {
        self.validate_contents();
    }

    #[template_callback]
    async fn on_create_btn_clicked(&self, _btn: gtk::Button) {
        let imp = self.imp();
        let dest_file = imp.file_button.selected_file().unwrap();
        let mut project_path: PathBuf = dest_file.path().unwrap();
        let project_type = imp.project_type.get();

        let project_name = imp.name_row.text();

        let project_name = match project_type {
            ProjectType::Symbolic => format!("{project_name}-symbolic.svg"),
            ProjectType::IconsSet => format!("{project_name}.svg"),
        };

        project_path.push(project_name);

        // TODO: handle the errors during project creation
        if let Ok(project) = Project::create(project_path, project_type).await {
            let parent = self.transient_for().and_downcast::<Window>().unwrap();
            let _ = project.open(self).await; // Open the project in the default svg editor/viewer
            parent.set_open_project(project);
        }
        self.destroy();
    }

    fn validate_contents(&self) {
        let imp = self.imp();
        let project_name = imp.name_row.text();
        let project_type = self.project_type();
        let selected_file = imp.file_button.selected_file();
        let is_valid =
            validate_project_name(&project_name, project_type) && selected_file.is_some();

        imp.create_btn.set_sensitive(is_valid);
    }
}
